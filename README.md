# Cornell Trading Competition

Jump to the "Using Our Server" section to jump straight into running a strategy with Apifiny Algo

## Registration

1. Registration will be handled by making an account on our server for each competitor. As detailed below, this is how submission will be handled.

2. To this end, participant email addresses should be collected by the organizers upon registration and sent to us so that we can set accounts up; this consists of adding a new user to the server that we are providing and adding the relevant folders.

## General details

1. The data for this competition is provided as if it came from a fictional exchange called TOUCOIN, with a fictional trading pair FXR/LIEN. We have provided both SWAP and SPOT data for this pair, over the 2 month period 2020-07-15 to 2020-09-15.
	1. As the competition has not yet started, only SPOT sample data from 2020-05-01 to 2020-06-30 has been provided.

2. Your strategy must trade ONLY using SPOT, however you are provided with SWAP to use as part of predictions should you choose to.

3. Strategies will be ranked solely on their PnL over the judging data.

4. Competitors will receive a summary of how their strategy performed on the judging data, as well as their rank among all other participants

5. Top competitors will receive more detailed information about how their strategy performed, including a graphic representation

## Setup

For the purpose of this competition, please use the following license info:

"license\_id":"TRAIL001",

"license\_key":"apifiny123456"

### On your own computer

It is advised to skip to the "Using our server" section if you wish to get started quickly with minimal difficulty.

#### Docker

> **Warning**
>
>
> Docker storage is NOT persistent by default. Additionally, to submit your code, you will have to rsync it to our server.
> Make sure to be mindful of this fact, and do not lose your hard work!

1. Download [Docker](https://docs.docker.com/get-docker/)

2. Run the Algo SDK docker image:
```
docker run -p 5555:5555 -it apifinyalgo/ctc
```
You may need to use sudo here, depending on OS.

> **${ALGO__HOME}**
>
>
> Your ALGO_HOME for this docker image will be /data/cc/algo_sdk, as that is where it is downloaded.
> The environment should be preconfigured, however it is worthy of not that you will find the sdk here.

3. Get started! See the "Writing your code" section for more details.

> **Jupyter**
> 
> If you want a GUI, you can use Jupyter!
> Just run the command```jupyter lab --allow-root --ip=0.0.0.0 --port=5555 --NotebookApp.token='TOKEN' --notebook-dir /home/dev```, with an arbitrary TOKEN (i.e. password) that you provide.
> Then, go to a web browser and visit the site ```localhost:5555```, and you'll see your Jupyter notebook!
> You can edit C++ files and also pull up a terminal!
> Standard warnings apply that this is NOT persistent, and that it is wise to bind-mount a directory or otherwise mount a volume to make sure you don't lose your hard work.


#### Ubuntu 20.04

1. To download Algo:
```
curl https://algoserver.apifiny.com/static/download/release/algo_sdk_1.1.0.tar.gz -o algo_sdk_1.1.0.tar.gz
tar -zxvf algo_sdk_1.1.0.tar.gz
```

2. Fetch our data via rsync ```rsync -av -e 'ssh -p 2023' maryjane@38.142.207.130:/prod/data/toucoin/ data/toucoin```. The password is "johndoe".

3. For a quickstart, clone this repository
``` git clone https://gitlab.com/apifiny-algo/cornell-trading-competition.git ```

4. Set up your environment as provided for in "Using our server", steps 3 and beyond.

5. Get started! See the "Writing your code" section for more details.

### Using our server

Apifiny will provide a server for use by you, the hackers, over the course of the competition. During the competition period, there will be a calendar to use to sign up to use the server, and each competitor will receive a separate user account. For now, we have provided the maryjane user account for your use; the password is "johndoe".

1. To login, type ```ssh -p 2023 maryjane@38.142.207.130``` into a terminal window, and provide the password "johndoe" when prompted.

2. In the home folder (use the ```ls``` command to show its contents), you will see a folder labelled "submission". Each competitor will have a copy of this in their home folders, and these will be used to submit code.

3. To set up the environment, run the following commands, replacing ALGO_HOME with the path to your algo_sdk (on our server, it is at /prod/algo_sdk):

```
export ALGO_HOME=/prod/algo_sdk
export TZ=UTC
export LD_LIBRARY_PATH=${ALGO_HOME}/bin:$LD_LIBRARY_PATH
export PATH=${ALGO_HOME}/bin:$PATH
export PATH=${ALGO_HOME}/scripts:$PATH
export PYTHONPATH=${ALGO_HOME}/scripts:$PYTHONPATH

export PATH=${HOME}/bin:$PATH
```

You may wish to save this to your ~/.bashrc (This has already been done for maryjane)

4. There is a default configuration file provided, maker.json. To run it, enter the command ```ccc_sim_trader examples/maker.json 20200501```.

5. To run your strategy over a date range, use ```gen_dates.py -sd 20200501 -ed 20200701 | parallel -j 64 ccc_sim_trader examples/maker.json```

6. To get summary statistics of how your strategy did, run ```sim_ana.py -sd 20200501 -ed 20200701 -p logs```


### Submission

1. To submit your strategy, place your C++ code (if applicable) and your configuration file into one folder on your own computer. We will assume you called it dir1.

2. Run the following command: ```rsync -a -e "ssh -p 2023" dir1 maryjane@38.142.207.130:~/submission/``` on your own computer.

3. Naming convention: Config files are to be named "[COMPETITOR_ID].json". C++ strategy files are to be named "[COMPETITOR_ID]-strat-[CLASS_NAME].cpp" and "[COMPETITOR_ID]-strat-[CLASS_NAME].h". Exactly one config file and one C++ strategy file (if applicable) can be submitted per competitor. If multiple are submitted, the most recently edited one(s) will be selected. If you wish to submit further C++ files (e.g. variables), make a folder within the "submission" folder called "xlibs" and place the compiled binary files there. 

## Writing your code

### Editing the JSON files

For detailed documentation, see algo.apifiny.com. In general terms, Variables do computations with market data, Pricing Models fetch the data, Samplers determine when to fetch new data, and Strategies take in a signal and then decide how to trade based on it. You should probably not edit any of the JSON file above the "variables" part unless you know what you are doing and have a good reason; it should in most cases be sufficient and necessary.

### Running your strategy

In general, you can run your strategy with the command ```ccc_sim_trader path/to/config/file date```, where the date is in the YYYYMMDD format. This applies if you haven't compiled your own C++ code; if you have, see below.

### C++ code

In the examples folder, you will find some C++ files. You can edit them and add your own. Once you have done so, to run your strategy:

1. in CCMain.cpp, import your strategy file.

2. In getStrategy, add a new condition to the if statement, with your strategy name in place of the default.

3. Build your strategy, by running in your base directory ```sh build_scripts/rebuild.sh```

4. Run your strategy using the same syntax as in the above section, but replace all instances of ```ccc_sim_trader``` with ```strat```.
